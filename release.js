// this script bumps the release version
const fs = require('fs');
const readline = require('readline');
const exec = require('child_process').exec;
const path = require('path');
const packagePath = path.join(__dirname, 'package.json');
const packageJson = fs.readFileSync(packagePath, 'utf-8');
var packageObj = JSON.parse(packageJson);

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

console.log(`Current version is ${packageObj.version}`);

rl.question('What version would you like to release? ', (answer) => {

  console.log(`Setting package.json to ${answer}`);
  packageObj.version = answer;
  fs.writeFileSync(packagePath, JSON.stringify(packageObj, null, 2), {
    encoding: 'utf-8'
  });

  console.log('Building release');
  exec('npm run build', (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    console.log(stdout);

    console.log('Commiting');
    exec(`git add . && git commit -m "Release ${answer}"`, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        return;
      }
      console.log(stdout);

      console.log(`Tagging commit at ${answer} and pushing to origin`);
      exec(`git tag -a v${answer} -m "v${answer}" && git push origin master --tags`, (error, stdout, stderr) => {
        if (error) {
          console.error(`exec error: ${error}`);
          return;
        }
        console.log(stdout);
        console.log('Done =)')
        process.exit(0);
      });
    });
  });

});